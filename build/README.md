# Build

## Prerequisites
* [CMake](https://cmake.org);
* [Make](https://gnu.org/software/make);
* A C++ compiler with C++17 support.

## Available targets
* default target: compile to lib;
* `context_stack_example`: compile to executable (executable is built in this
folder, as `example/context_stack_example`).

## Build instructions
### Linux/Mac
#### First compilation
```sh
cmake ..
make <target name>
```

#### Subsequent compilations
```sh
make <target name>
```
