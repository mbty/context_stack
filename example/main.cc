// This file is part of the context stack library -
// gitlab.com/mbty/context_stack
#include "../src/context_stack.hh"
#include <iostream>
namespace cs = context_stack;

int main() {
  // Put "main loop" on the stack, with a counter going up to 3 initialized at
  // 1.
  auto lc = cs::local_context("main loop", 3);
  {
    // Put "doing stuff" on the stack, with a counter going up to 2 initialized
    // at 1.
    auto lc = cs::local_context("doing stuff", 2);
    // Display the context stack
    std::cout << cs::context << " - something" << std::endl;
    // Increment "doing stuff" counter from 1 to 2.
    ++lc;
    {
      // This counter is initialized with a maximum of 0, and thus ignored.
      auto lc = cs::local_context("something else", 0);
      std::cout << cs::context << std::endl;
      std::cout << "Exception text example: " << cs::generate_error_string()
        << "::exception_name" << std::endl;
    }
    // lc (not the main one which is shadowed by the inner one) goes out of
    // scope and is popped from the stack.
  }
  {
    // When dealing with parallel code, standard counters might not cut it.
    // A custom counter value can be defined with the following syntax:
    auto lc = cs::local_context("doing stuff", "1A/3");
    std::cout << cs::context << " - more work" << std::endl;
    std::cout << "Exception text example: " << cs::generate_error_string()
      << "::exception_name" << std::endl;
    // Custom counters consist of arbitrary text and have to be managed entirely
    // by hand. There is no update function since I don't see a usecase for it
    // in the time being.
  }
  ++lc;
  std::cout << cs::context << " - more work" << std::endl;
  ++lc;
  std::cout << cs::context << " - more work still" << std::endl;
}
