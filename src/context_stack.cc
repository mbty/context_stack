// This file is part of the context stack library -
// gitlab.com/mbty/context_stack
#include "context_stack.hh"
#include <algorithm>
#include <cctype>
#include <string_view>
namespace context_stack {
  local_context::local_context(
    std::string &&description, const unsigned int counter_max
  ) {
    context.emplace_back(std::move(description), "", 1, counter_max);
  }

  local_context::local_context(
    const std::string &description, const unsigned int counter_max
  ) {
    context.emplace_back(description, "", 1, counter_max);
  }

  local_context::local_context(
    std::string &&description, std::string &&custom_counter
  ) {
    context.emplace_back
      (std::move(description), std::move(custom_counter), 0, 0);
  }

  local_context::local_context(
    const std::string &description, const std::string &custom_counter
  ) {
    context.emplace_back(description, custom_counter, 0, 0);
  }

  local_context::~local_context() {
    context.pop_back();
  }

  // # Operators
  local_context &local_context::operator++() {
    context[context.size() - 1].counter_state += 1;
    return *this;
  }

  std::ostream &operator<<(
    std::ostream &os, [[maybe_unused]] const dummy &dummy
  ) {
    // Return early to avoid if the context stack is empty.
    // Existence of at least one element is assumed in the afterwards.
    if (local_context::context.size() == 0) {
      return os;
    }

    auto &cs = local_context::context;

    if (cs[0].description.size() > 1) {
      // Display the first element of the context separately since the first
      // letter of its description needs to be capitalized.
      os << static_cast<char>(std::toupper(cs[0].description[0]))
        << std::string_view(cs[0].description.c_str() + 1);
      // TODO replace back with line below once C++20 support commonplace enough
      // cs[0].description.cbegin() + 1, cs[0].description.cend()
      // Mind the fact that the counter need not be displayed when
      // counter_max < 2.
    }
    if (cs[0].counter_max == 0) {
      os << " (" << cs[0].custom_counter << ")";
    }
    else if (cs[0].counter_max > 1) {
      os << " (" << cs[0].counter_state << "/" << cs[0].counter_max << ")";
    }

    // Then display all other elements in almost the same way
    for (auto it = cs.cbegin() + 1; it != cs.cend(); ++it) {
      os << " - " << it->description;
      if (it->counter_max == 0) {
        os << " (" << it->custom_counter << ")";
      }
      else if (it->counter_max > 1) {
        os << " (" << it->counter_state << "/" << it->counter_max << ")";
      }
    }

    return os;
  }

  // # Other functions
  // Assumption: reasonable strings size (concatenation does not result in an
  // overflow). Still, can't make noexcept.
  std::string generate_error_string() {
    auto &cs = local_context::context;

    // Return early to avoid if the context stack is empty.
    // Existence of at least one element is assumed in the afterwards.
    if (cs.size() == 0) {
      return "";
    }

    // Precompute the length of the string to avoid reallocations.
    std::string result;
    size_t context_description_length = 0;
    for (const auto &item : cs) {
      context_description_length += item.description.size();
    }
    result.reserve(context_description_length + 2*(cs.size() - 1));

    // Build the string.
    result.append(cs[0].description);
    for (auto it = cs.cbegin() + 1; it != cs.cend(); ++it) {
      result.append("::");
      result.append(it->description);
    }

    // Format the result.
    std::replace(result.begin(), result.end(), ' ', '_');

    return result;
  }
}
