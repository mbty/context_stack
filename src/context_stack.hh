// This file is part of the context stack library -
// gitlab.com/mbty/context_stack
#ifndef CONTEXT__STACK_CONTEXT__STACK_HH
#define CONTEXT__STACK_CONTEXT__STACK_HH
#include <ostream>
#include <string>
#include <vector>
namespace context_stack {
  // The context object can be passed to a stream to display the local state of
  // the context stack.
  inline const class dummy { } context = {};

  // This class contains the context_stack, and is used to access its contents
  // in a controlled fashion.
  class local_context {
    public: // Functions
      local_context(std::string &&description, unsigned int counter_max);
      local_context(const std::string &description, unsigned int counter_max);
      local_context(std::string &&description, std::string &&custom_counter);
      local_context
        (const std::string &description, const std::string &custom_counter);
      ~local_context();

      // # Operators
      local_context &operator++();

    private: // Members
      // context_atom is the building brick of the context stack.
      // It contains a text description of the context, as well as a counter.
      struct context_atom {
        const std::string description;
        const std::string custom_counter;
        unsigned int counter_state;
        const unsigned int counter_max;

        // TODO remove once C++20 support commonplace enough
        context_atom(
          const std::string &_description, const std::string &_custom_counter,
          unsigned int _counter_state, unsigned int _counter_max
        )
        : description(_description), custom_counter(_custom_counter),
          counter_state(_counter_state), counter_max(_counter_max)
        { }
      };

      inline static thread_local std::vector<context_atom> context = {};

    public: // Friends
      friend std::ostream &operator<<(std::ostream &os, const dummy &dummy);
      friend std::string generate_error_string();
  };
  std::ostream &operator<<(std::ostream &os, const dummy &dummy);

  // Assumption: reasonable strings size (concatenation does not result in an
  // overflow). Still, can't make noexcept.
  std::string generate_error_string();
}
#endif
