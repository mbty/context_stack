# Context stack
A simple (less than 100 LOC) and thread-safe context stack for contextualized
log messages and exceptions.

## Features
* context definition and restitution;
* plays nicely with streams;
* exception string generation;
* simple (< 100 LOC);
* thread-safe (context defined as `thread_local`).

## Usage
If you want to quickly understand what this library is about, see the contents
of the `example` folder directly.

The contents of this library is defined inside the `context_stack` namespace.

The context is a stack of `context_atom`s.
Each `context_atom` contains both a description string and a progress counter
(of the form `x/y`).
The description is used both for information messages and exceptions, whereas
the complement is only used for the former.

The `local_context` constructors are used to expand the context:
```C++
local_context(std::string &&description, unsigned int counter_max);
local_context(const std::string &description, unsigned int counter_max);
```

On destruction, a `local_context` pops the stack.

The counter is initially in the state 1/`counter_max`.
`counter_max` can be defined as `0` or `1` if not used. It is incremented by
applying the `++` prefix operator on a local_context.

To display the contents of the stack, use the `<<` operator with the `context`
object defined in `context_stack.hh`:
```C++
std::cout << context_stack::context << std::endl;
```

This could result in the following output:
```
Main loop (1/3) - doing stuff (2/2) - something else
```

Formatting happens as follows:
* `context_atom`s are separated by ` - `;
* when `counter_max` is defined as something greater than `1` for a given
`context_atom`, it is displayed between parentheses separated from the
description by a space;
* when `counter_max` is defined as `0` or `1`, it is ignored;
* the first letter of the first `context_atom` is capitalized;
* that's all.

This library can also be used to generate exception messages. The function made
for this purpose follows:
```C++
std::string generate_error_string();
```

It could result in the following string:
`main_loop::doing_stuff::something_else`. This string could then be expanded
with some more information regarding the precise nature of the exception at
hand.

Note that this function is not defined as `noexcept`. The reason is that it
handles strings, which entails some behind-the-scenes dynamic allocation in
C++. For almost all practical purposes, it can be considered `noexcept`.

## Dependencies
None.

## Build
See instructions in the README.md found in the build folder.

## Legal
This library is free software. It comes without any warranty, to the extent
permitted by applicable law. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2, included
in the LICENSE.md file.
